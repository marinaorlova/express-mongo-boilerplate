// require("app-module-path").addPath(path.resolve(__dirname, "../"));
require("app-module-path").addPath(__dirname);
const log = require("app/logger").create("Server");
const app = require("app/express");

const { SCHEME, PORT } = process.env;
const server = require(SCHEME).createServer(app);

server.listen(PORT, () => log.info(`Listening on port ${PORT}`));

// Initialize MongoDb connection
const { stopMongoClient } = require("app/mongo")();

const gracefulExit = () => {
  // If the Node process ends, close the Mongoose connection
  log.info("Application is being terminated");
  stopMongoClient();
};

process.on("SIGINT", gracefulExit).on("SIGTERM", gracefulExit);

process.on("unhandledRejection", (reason, promise) => {
  log.error("Unhandled Rejection at:", reason.stack || reason);
  // todo Send the information to sentry.io
  process.exit(1);
});
