const _ = require("lodash");
const winston = require("winston");
require("winston-daily-rotate-file");
const config = require("./config");

const logger = winston.createLogger({
  transports: [
    new winston.transports.Console({
      colorize: true,
      level: config.loggerLevel
    })
  ],
  exceptionHandlers: [
    new winston.transports.Console({ level: "error" }),
    new winston.transports.File({
      filename: config.loggerFile || "./exceptions.log",
      level: config.loggerLevel
    })
  ]
});

if (config.loggerFile) {
  logger.add(
    new winston.transports.DailyRotateFile({
      filename: `${config.loggerFile}-%DATE%.log`,
      datePattern: "YYYY-MM-DD",
      // zippedArchive: true,
      maxSize: "10m",
      maxFiles: "7d"
    })
  );
}

function create(name) {
  function format(level, args) {
    logger[level](`${name} at ${new Date()}: ${args.shift()}`, ...args);

    const error = _.last(args);
    if (error instanceof Error && error.response && error.code) {
      logger[level](`${name}: Response code: ${error.code}`);
    }
  }
  return {
    debug(...args) {
      format("debug", args);
    },
    info(...args) {
      format("info", args);
    },
    warn(...args) {
      format("warn", args);
    },
    error(...args) {
      format("error", args);
    }
  };
}

module.exports = {
  create
};
