/* eslint-disable max-classes-per-file */
const boom = require("@hapi/boom");
const {isCelebrate} = require("celebrate");

class HttpError extends Error {
  constructor(message, error) {
    super(message);
    // all unrecognized errors become internal 500
    // in production environment boom will not print out the carried message for 500
    if (boom.isBoom(error)) {
      this.httpError = error;
    } else if (isCelebrate(error)) {
      // validation error
      this.httpError = boom.badRequest(error.message);
    } else if (error instanceof Error) {
      this.httpError = boom.boomify(error, { statusCode: 500 });
    } else {
      this.httpError = boom.internal(message);
    }
  }

  status() {
    return this.httpError.output.statusCode;
  }

  json() {
    return { ...this.httpError.output.payload };
  }

  log() {
    return {
      ...this.httpError.output.payload,
      data: this.httpError.data,
      stack: this.httpError.stack,
      message: this.message
    };
  }
}

class AuthNotConfigured extends HttpError {
  constructor(message, data) {
    super(message, boom.forbidden("Auth method not configured", data));
  }
}

class Unauthorized extends HttpError {
  constructor(message, data) {
    super(message, boom.unauthorized("Can not authenticate user", data));
  }
}

class InvalidSignature extends HttpError {
  constructor(message, data) {
    super(message, boom.unauthorized("Invalid signature", data));
  }
}

class InvalidToken extends HttpError {
  constructor(message, data) {
    super(message, boom.unauthorized("Invalid token", data));
  }
}

class NotFound extends HttpError {
  constructor(message) {
    super(message, boom.notFound());
  }
}

class DBError extends HttpError {
  constructor(message, data) {
    super(message, boom.internal(message, data));
  }
}

class ValidationError extends HttpError {
  constructor(message, errors) {
    super(message, boom.badRequest(JSON.stringify(errors)));
  }
}

const normalizeError = err => {
  if (err instanceof HttpError) {
    return err;
  }

  // any unknown error gets becomes http error 500
  return new HttpError(err.message, err);
};

module.exports = {
  AuthNotConfigured,
  ValidationError,
  InvalidToken,
  InvalidSignature,
  NotFound,
  DBError,
  HttpError,
  Unauthorized,
  normalizeError
};
