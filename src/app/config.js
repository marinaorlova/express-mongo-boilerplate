const camelcase = require("camelcase");

// dotenv-flow.config() does PARSE and MERGE .env.* files and appends the resulting vars to process.env
// (append, not override, so that an existing process.env.VAR takes priority)
// Thus we don't mind parsed values, only the process.env values matter as a result
// but we need keys of the parsed vars to know which env are to be included in the app config
const parsedKeys = Object.keys(require("dotenv-flow").config().parsed);

// existing process.env are not overriten by dotenv-flow take priority over
const config = parsedKeys.reduce((acc, key) => {
  acc[camelcase(key)] = process.env[key];
  return acc;
}, {});

module.exports = {
  ...config
};
