const express = require('express');
require('express-async-errors');
require("./config");
const log = require("./logger").create("Application");

const initErrorHandler = app => {
  const { normalizeError } = require("./errors");

  app.use((err, req, res, next) => {
    if (!err) {
      return next();
    }

    const error = normalizeError(err);
    const statusCode = error.status();

    if (statusCode >= 400 && statusCode < 500) {
      log.warn(`Client error while handling ${req.url}:`, error.log());
    } else {
      log.error(`Exception while handling ${req.url}:`, error.log());
    }

    res.status(statusCode).json(error.json());
  });
};

const initMiddleware = (app) => {
  const api = require("routes");
  const bodyParser = require("body-parser");

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));

  app.use(
    "/api",
    api.router,
    api.notFound
  );
};

const app = express();
initMiddleware(app);
initErrorHandler(app);

module.exports = app;
