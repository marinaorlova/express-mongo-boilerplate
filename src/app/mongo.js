const mongoose = require("mongoose");
const log = require("./logger").create("Mongo");

const { mongoConnection } = require("./config");

if (!mongoConnection) {
  throw Error("Missing Mongo connection parameters");
}

module.exports = () => {
  mongoose.connect(mongoConnection, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true
  });

  mongoose.pluralize(null);

  const db = mongoose.connection;

  db.once("connected", () => {
    log.info("MongoDB connected");
  });

  db.on("error", error => {
    throw error;
  });

  const stopMongoClient = () => {
    db.close(() => {
      log.info(`Mongoose default connection is disconnected`);
      process.exit(0);
    });
  };

  return { stopMongoClient };
};
