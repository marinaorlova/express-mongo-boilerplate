const { NotFound } = require("app/errors");

function notFound(req, res, next) {
  next(new NotFound());
}

module.exports = notFound;
