const express = require("express");
const { celebrate, errors } = require("celebrate");

const test = require("./test.js");
const notFound = require("./notFound.js");

const apiRouter = new express.Router();
const validateOptions = { abortEarly: false };

apiRouter.post(
  "/test",
  celebrate(test.postValidationSchema, validateOptions),
  test.post
);
apiRouter.get("/test", test.get);

module.exports = {
  router: apiRouter,
  notFound
};
