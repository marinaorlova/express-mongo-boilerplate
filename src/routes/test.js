const { Joi } = require("celebrate");

// test it from cli:
// curl -XPOST -H "Content-Type: application/json" -d '{"credentials": "sss", "concept": 111}' localhost:5050/api/test
function post(req, res, next) {
  res.status(201).send({ status: "Resource created", request: req.body });
}

const postValidationSchema = {
  body: Joi.object().keys({
    credentials: Joi.string().required(),
    concept: Joi.number().required()
  })
};

// test it from cli:
// curl -XGET http://localhost:5050/api/test
function get(req, res, next) {
  res.json({ concept: 1, credentials: "sdfsd" });
}

module.exports = {
  post,
  get,
  postValidationSchema
};
