module.exports = {
  env: {
    node: true
  },
  extends: [
    "airbnb-base",
    "plugin:jest/recommended",
    "plugin:prettier/recommended"
  ],
  plugins: ["jest", "prettier"],
  rules: {
    "consistent-return": "off",
    "global-require": "off",
    "import/no-unresolved": "off",
    "import/no-extraneous-dependencies": "off",
    "import/no-dynamic-require": "off",
    "no-plusplus": 'off',
    "no-await-in-loop": "off",
    "no-unused-expressions": "off",
    "no-param-reassign": [
      "error",
      {
        props: true,
        ignorePropertyModificationsFor: ["acc", "ctx", "req", "res"]
      }
    ],
    "no-underscore-dangle": "off",
    "prettier/prettier": "error"
  }
};
